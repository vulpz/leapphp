<!DOCTYPE html>
<html lang='en'>

	<head>
		<link type='text/css' rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/bulma/0.5.3/css/bulma.min.css' />
		<link type='text/css' rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' />
		<style type='text/css'>
			html {
				background-color: #f5f5f5;
			}
			form {
				margin-left: auto;
				margin-right: auto;
				max-width: 340px;
			}
			button, button:focus {
				border: none !important;
			}
			button:active {
				background-color: #b5b5b5;
			}
		</style>
		<title>LEAP Login</title>
	</head>

	<body>
		<section class='section'>
			<div class='container content has-text-centered'>
				<form class='box' method='POST' action='/'>
					{{ csrf_field() }}
					<figure class='image'>
						<img src='/img/loginlogo.png' />
					</figure>
					<div class='field'>
						<div class='control has-icons-left'>
							<input class='input' type='text' for='LoginForm' name='username' placeholder='Username' />
							<span class='icon is-small is-left'>
								<i class='fa fa-user'></i>
							</span>
						</div>
					</div>
					<div class='field'>
						<div class='control has-icons-left'>
							<input class='input' type='password' for='LoginForm' name='password' placeholder='Password' />
							<span class='icon is-small is-left'>
								<i class='fa fa-unlock-alt'></i>
							</span>
						</div>
					</div>
					<input type='hidden' for='LoginForm' name='source' value='mobile' />
					<div class='field'>
						<button class='button' for='LoginForm' name='submit' value='submit'>
							Log in
						</button>
						<span> <?php echo $status ?> </span>
					</div>
					<figure class='image'>
						<img src='/img/frog.jpg' />
					</figure>
				</form>
			</div>
		</section>
	</body>

</html>
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseUser;
use Parse\ParseClient;

class LoginController extends Controller
{

	public function showLogin() {
		
		return view('index', [
			'status' => ''
		]);
	}

	public function doLogin(Request $request) {

		// Save request creds.
		$user = $request->username;
		$pass = $request->password;
		$src = $request->source;

		// Login endpoint.
		$login_stream = curl_init('https://apitest.leaptodigital.com/parse/functions/loginUser');

		// Headers.
		$headers = array(
			'X-Parse-Application-Id: 123456', 
			'X-Parse-Session-Token: '
		);

		// cURL options.
		$opts = array(
			CURLOPT_HTTPHEADER => $headers,
			CURLOPT_POST => 1,
			CURLOPT_POSTFIELDS => 'username=' . $user .'&password=' . $pass . '&source=' . $src,
			CURLOPT_RETURNTRANSFER => true			
		);

		// Attach options.
		curl_setopt_array($login_stream, $opts);

		// Initiate curl call and save response.
		$login_response = curl_exec($login_stream);

		// Decode JSON string to stdObject.
		if($login_result = (array) json_decode($login_response))
		{

			// Pull session token for authentication.
			if($token = $login_result['result']->result->sessionToken)
			{

				// Close curl stream.
				curl_close($login_stream);

				session(['sessionToken' => $token]);

				LoginController::queryParse(session('sessionToken'));

				return view('viewData');
			}
			else
			{
				return redirect('/', [
					'status' => 'Bad login. Please try again.'
				]);
			}
		}
		else
		{

			return redirect('/', [
				'status' => 'Bad login. Please try again.'
			]);
		}
	}

	public function queryParse($token) {

		// Initialze Parse id, key, and endpoint.
		ParseClient::initialize('123456', null, 'abcdefg');
		ParseClient::setServerURL('https://apitest.leaptodigital.com', 'parse');
		ParseUser::become($token);

		// Create Parse query for specified object.
		$p_query = new ParseQuery('SSMeasureSheetItem');

		// Query Parse server.
		$p_q_results = $p_query->find();

		//var_dump($p_q_results);
		echo '<pre>' . print_r((array)$p_q_results, true) . '</pre>';
	}
}

<?php

require '.\\vendor\\autoload.php';
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseUser;
use Parse\ParseClient;

class QueryParser
{
	public function queryParse($token) {

		// Initialze Parse id, key, and endpoint.
		ParseClient::initialize('123456', null, 'abcdefg');
		ParseClient::setServerURL('https://apitest.leaptodigital.com', 'parse');
		ParseUser::become($token);

		// Create Parse query for specified object.
		$p_query = new ParseQuery('SSMeasureSheetItem');

		// Query Parse server.
		$p_q_results = $p_query->find();

		print_r($p_q_results);
	}
}
